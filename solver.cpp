/* compute optimal solutions for sliding block puzzle. */
#include <SDL2/SDL.h>
#include <stdio.h>
#include <cstdlib>   /* for atexit() */
#include <algorithm>
using std::swap;
#include <iostream>
using namespace std;
#include <cassert>
#include <vector>
#include <set>
#include <queue>

/* SDL reference: https://wiki.libsdl.org/CategoryAPI */

/* initial size; will be set to screen size after window creation. */
int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;
int fcount = 0;
int mousestate = 0;
SDL_Point lastm = {0,0}; /* last mouse coords */
SDL_Rect bframe; /* bounding rectangle of board */
static const int ep = 2; /* epsilon offset from grid lines */

bool init(); /* setup SDL */
void initBlocks();
void updateGameState();
void configureGameState(uint64_t gs);
void configureGameStateLite(uint64_t gs);
void printConf();

//uint64_t initialGS = 604809471578678466;
uint64_t initialGS = 369948418003281114;

#define FULLSCREEN_FLAG SDL_WINDOW_FULLSCREEN_DESKTOP
// #define FULLSCREEN_FLAG 0

/* NOTE: ssq == "small square", lsq == "large square" */
enum bType {hor,ver,ssq,lsq};
struct block {
	SDL_Rect R; /* screen coords + dimensions */
	bType type; /* shape + orientation */
	/* TODO: you might want to add other useful information to
	 * this struct, like where it is attached on the board.
	 * (Alternatively, you could just compute this from R.x and R.y,
	 * but it might be convenient to store it directly.) */
	int locX = -1;
	int locY = -1;
	int pos = -1;

	void rotate() /* rotate rectangular pieces */
	{
		if (type != hor && type != ver) return;
		type = (type==hor)?ver:hor;
		swap(R.w,R.h);
	}
};

#define NBLOCKS 10
block B[NBLOCKS];
block* dragged = NULL;

struct gameState {
	vector<uint64_t> path;
	uint64_t id = 0;
	vector<uint64_t> edges;
};

gameState currentGS;
int conf[20];

set<uint64_t> D;			//discovered
queue<uint64_t> SE;		//search edges
vector<uint64_t> DE;	//this is redundant to D but parallel with PE
vector<uint64_t> PE;	//stores parent state of each newly discovered edge
bool won = false;
uint64_t originalState;
uint64_t winningState;
size_t step = 0;

vector<uint64_t> path;

block* findBlock(int x, int y);
void close(); /* call this at end of main loop to free SDL resources */
SDL_Window* gWindow = 0; /* main window */
SDL_Renderer* gRenderer = 0;

void findMoves();
void addEdge(block* b, int newPos);

bool init()
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_Init failed.  Error: %s\n", SDL_GetError());
		return false;
	}
	/* NOTE: take this out if you have issues, say in a virtualized
	 * environment: */
	if(!SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1")) {
		printf("Warning: vsync hint didn't work.\n");
	}
	/* create main window */
	gWindow = SDL_CreateWindow("Sliding block puzzle solver",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH, SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN|FULLSCREEN_FLAG);
	if(!gWindow) {
		printf("Failed to create main window. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	/* set width and height */
	SDL_GetWindowSize(gWindow, &SCREEN_WIDTH, &SCREEN_HEIGHT);
	/* setup renderer with frame-sync'd drawing: */
	gRenderer = SDL_CreateRenderer(gWindow, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(!gRenderer) {
		printf("Failed to create renderer. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	SDL_SetRenderDrawBlendMode(gRenderer,SDL_BLENDMODE_BLEND);

	for(int i = 0; i < 20; i++)
		conf[i] = -1;

	initBlocks();
	return true;
}

/* TODO: you'll probably want a function that takes a state / configuration
 * and arranges the blocks in accord.  This will be useful for stepping
 * through a solution.  Be careful to ensure your underlying representation
 * stays in sync with what's drawn on the screen... */

void initBlocks()
{
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int h = H*3/4;
	int w = 4*h/5;
	int u = h/5-2*ep;
	int mw = (W-w)/2;
	int mh = (H-h)/2;

	/* setup bounding rectangle of the board: */
	bframe.x = (W-w)/2;
	bframe.y = (H-h)/2;
	bframe.w = w;
	bframe.h = h;

	/* NOTE: there is a tacit assumption that should probably be
	 * made explicit: blocks 0--4 are the rectangles, 5-8 are small
	 * squares, and 9 is the big square.  This is assumed by the
	 * drawBlocks function below. */

	for (size_t i = 0; i < 5; i++) {
		B[i].R.x = (mw-2*u)/2;
		B[i].R.y = mh + (i+1)*(u/5) + i*u;
		B[i].R.w = 2*(u+ep);
		B[i].R.h = u;
		B[i].type = hor;
		B[i].locX = -1;
		B[i].locY = -1;
		B[i].pos = -1;
	}

	/*
	B[4].R.x = mw+ep;
	B[4].R.y = mh+ep;
	B[4].R.w = 2*(u+ep);
	B[4].R.h = u;
	B[4].type = hor;
	*/

	/* small squares */
	for (size_t i = 0; i < 4; i++) {
		B[i+5].R.x = (W+w)/2 + (mw-2*u)/2 + (i%2)*(u+u/5);
		B[i+5].R.y = mh + ((i/2)+1)*(u/5) + (i/2)*u;
		B[i+5].R.w = u;
		B[i+5].R.h = u;
		B[i+5].type = ssq;
		B[i+5].locX = -1;
		B[i+5].locY = -1;
		B[i+5].pos = -1;
	}
	B[9].R.x = B[5].R.x + u/10;
	B[9].R.y = B[7].R.y + u + 2*u/5;
	B[9].R.w = 2*(u+ep);
	B[9].R.h = 2*(u+ep);
	B[9].type = lsq;
	B[9].locX = -1;
	B[9].locY = -1;
	B[9].pos = -1;
}

void drawBlocks()
{
	/* rectangles */
	SDL_SetRenderDrawColor(gRenderer, 0x43, 0x4c, 0x5e, 0xff);
	for (size_t i = 0; i < 5; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* small squares */
	SDL_SetRenderDrawColor(gRenderer, 0x5e, 0x81, 0xac, 0xff);
	for (size_t i = 5; i < 9; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* large square */
	SDL_SetRenderDrawColor(gRenderer, 0xa3, 0xbe, 0x8c, 0xff);
	SDL_RenderFillRect(gRenderer,&B[9].R);
}

/* return a block containing (x,y), or NULL if none exists. */
block* findBlock(int x, int y)
{
	/* NOTE: we go backwards to be compatible with z-order */
	for (int i = NBLOCKS-1; i >= 0; i--) {
		if (B[i].R.x <= x && x <= B[i].R.x + B[i].R.w &&
				B[i].R.y <= y && y <= B[i].R.y + B[i].R.h)
			return (B+i);
	}
	return NULL;
}

void close()
{
	SDL_DestroyRenderer(gRenderer); gRenderer = NULL;
	SDL_DestroyWindow(gWindow); gWindow = NULL;
	SDL_Quit();
}

void render()
{
	/* draw entire screen to be black: */
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(gRenderer);

	/* first, draw the frame: */
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int w = bframe.w;
	int h = bframe.h;
	SDL_SetRenderDrawColor(gRenderer, 0x39, 0x39, 0x39, 0xff);
	SDL_RenderDrawRect(gRenderer, &bframe);
	/* make a double frame */
	SDL_Rect rframe(bframe);
	int e = 3;
	rframe.x -= e;
	rframe.y -= e;
	rframe.w += 2*e;
	rframe.h += 2*e;
	SDL_RenderDrawRect(gRenderer, &rframe);

	/* draw some grid lines: */
	SDL_Point p1,p2;
	SDL_SetRenderDrawColor(gRenderer, 0x19, 0x19, 0x1a, 0xff);
	/* vertical */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x;
	p2.y = p1.y + h;
	for (size_t i = 1; i < 4; i++) {
		p1.x += w/4;
		p2.x += w/4;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	/* horizontal */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x + w;
	p2.y = p1.y;
	for (size_t i = 1; i < 5; i++) {
		p1.y += h/5;
		p2.y += h/5;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	SDL_SetRenderDrawColor(gRenderer, 0xd8, 0xde, 0xe9, 0x7f);
	SDL_Rect goal = {bframe.x + w/4 + ep, bframe.y + 3*h/5 + ep,
	                 w/2 - 2*ep, 2*h/5 - 2*ep};
	SDL_RenderDrawRect(gRenderer,&goal);

	/* now iterate through and draw the blocks */
	drawBlocks();
	/* finally render contents on screen, which should happen once every
	 * vsync for the display */
	SDL_RenderPresent(gRenderer);
}

void snap(block* b)
{
	/* TODO: once you have established a representation for configurations,
	 * you should update this function to make sure the configuration is
	 * updated when blocks are placed on the board, or taken off.  */
	assert(b != NULL);

	/* upper left of grid element (i,j) will be at
	 * bframe.{x,y} + (j*bframe.w/4,i*bframe.h/5) */
	/* translate the corner of the bounding box of the board to (0,0). */

	if(b->pos != -1)
	{
		conf[b->pos] = -1;
		if(b->type == hor || b->type == lsq) {conf[b->pos + 1] = -1;}
		if(b->type == ver || b->type == lsq) {conf[b->pos + 4] = -1;}
		if(b->type == lsq) {conf[b->pos + 5] = -1;}
	}

	int x = b->R.x - bframe.x;
	int y = b->R.y - bframe.y;
	int uw = bframe.w/4;
	int uh = bframe.h/5;
	/* NOTE: in a perfect world, the above would be equal. */
	int i = (y+uh/2)/uh; /* row */
	int j = (x+uw/2)/uw; /* col */

	if (0 <= i && i < 5 && 0 <= j && j < 4) {
		b->R.x = bframe.x + j*uw + ep;
		b->R.y = bframe.y + i*uh + ep;

		b->locX = j;
		b->locY = i;
		b->pos = 4*i + j;

		conf[4*i + j] = b->type;
		if(b->type == hor || b->type == lsq) {conf[4*i + (j+1)] = b->type;}
		if(b->type == ver || b->type == lsq) {conf[4*(i+1) + j] = b->type;}
		if(b->type == lsq) {conf[4*(i+1) + (j+1)] = b->type;}
	}
	else {
		b->locX = -1;
		b->locY = -1;
		b->pos = -1;
	}

	updateGameState();

}

void updateGameState()
{
	currentGS.id = 8;
	for(size_t i = 0; i < 20; i++)
	{
		if(conf[i] != -1)
			currentGS.id = currentGS.id + conf[i] + 1;

		if(i != 19)
			currentGS.id *= 8;
	}
	currentGS.id -= 1152921504606846976;
}

void configureGameState(uint64_t gs)
{
	initBlocks();
	currentGS.id = gs;
	currentGS.edges.clear();
	for(int i = 0; i < 20; i++)
		conf[i] = -1;

	size_t rOffset = 0;
	size_t sOffset = 0;

	for(size_t i = 20; i > 0; i--)
	{
		size_t p = gs % 8;
		conf[i-1] = p;
		conf[i-1]--;

		gs /= 8;
	}

	int uw = bframe.w/4;
	int uh = bframe.h/5;
	block* b;
	bool placedSquare = false;

	for(size_t i = 0; i < 20; i++)
	{
		if(conf[i] == 0 || conf[i] == 2 || (conf[i] == 3 && !placedSquare))
		{
			if(conf[i] == 0) {b = &B[rOffset]; rOffset++;}
			if(conf[i] == 2) {b = &B[5 + sOffset]; sOffset++;}
			if(conf[i] == 3 && !placedSquare) {b = &B[9]; placedSquare = true;}
			b->locX = i % 4;
			b->locY = i / 4;
			b->pos = i;
			b->R.x = bframe.x + b->locX*uw + ep;
			b->R.y = bframe.y + b->locY*uh + ep;
			if(conf[i] == 0) {i++;}
		}
	}

	for(size_t x = 0; x < 4; x++)
		for(size_t y = 0; y < 4; y++)
			if(conf[4*y + x] == 1)
			{
				b = &B[rOffset];
				b->rotate();
				rOffset++;
				int i = 4*y + x;
				b->locX = i % 4;
				b->locY = i / 4;
				b->pos = i;
				b->R.x = bframe.x + b->locX*uw + ep;
				b->R.y = bframe.y + b->locY*uh + ep;
				y++;
			}
}

void configureGameStateLite(uint64_t gs)
{
	initBlocks();
	currentGS.id = gs;
	currentGS.edges.clear();
	for(int i = 0; i < 20; i++)
		conf[i] = -1;

	size_t rOffset = 0;
	size_t sOffset = 0;

	for(size_t i = 20; i > 0; i--)
	{
		size_t p = gs % 8;
		conf[i-1] = p;
		conf[i-1]--;

		gs /= 8;
	}

	block* b;
	bool placedSquare = false;

	for(size_t i = 0; i < 20; i++)
	{
		if(conf[i] == 0 || conf[i] == 2 || (conf[i] == 3 && !placedSquare))
		{
			if(conf[i] == 0) {b = &B[rOffset]; rOffset++;}
			if(conf[i] == 2) {b = &B[5 + sOffset]; sOffset++;}
			if(conf[i] == 3 && !placedSquare) {b = &B[9]; placedSquare = true;}
			b->locX = i % 4;
			b->locY = i / 4;
			b->pos = i;
			if(conf[i] == 0) {i++;}
		}
	}

	for(size_t x = 0; x < 4; x++)
		for(size_t y = 0; y < 4; y++)
			if(conf[4*y + x] == 1)
			{
				b = &B[rOffset];
				b->rotate();
				rOffset++;
				int i = 4*y + x;
				b->locX = i % 4;
				b->locY = i / 4;
				b->pos = i;
				y++;
			}
}

void findMoves()
{
	for(size_t bi = 0; bi < 10; bi++)
	{
		block* b = &B[bi];

		if(b->pos != -1)
		{
			if(b->type == ver){
				if(b->locY>0 && conf[b->pos-4]==-1){
					addEdge(b, b->pos-4);
					if(b->locY>1 && conf[b->pos-8]==-1){
						addEdge(b, b->pos-8);
						if(b->locY>2 && conf[b->pos-12]==-1)
							addEdge(b, b->pos-12);}}

				if(b->locY<3 && conf[b->pos+8]==-1){
					addEdge(b, b->pos+4);
					if(b->locY<2 && conf[b->pos+12]==-1){
						addEdge(b, b->pos+8);
						if(b->locY<1 && conf[b->pos+16]==-1)
							addEdge(b, b->pos+12);}}

				if(b->locX>0 && conf[b->pos-1]==-1 && conf[b->pos+3]==-1){
					addEdge(b, b->pos-1);
					if(b->locX>1 && conf[b->pos-2]==-1 && conf[b->pos+2]==-1){
						addEdge(b, b->pos-2);
						if(b->locX>2 && conf[b->pos-3]==-1 && conf[b->pos+1]==-1)
							addEdge(b, b->pos-3);}}

				if(b->locX<3 && conf[b->pos+1]==-1 && conf[b->pos+5]==-1){
					addEdge(b, b->pos+1);
					if(b->locX<2 && conf[b->pos+2]==-1 && conf[b->pos+6]==-1){
						addEdge(b, b->pos+2);
						if(b->locX<1 && conf[b->pos+3]==-1 && conf[b->pos+7]==-1)
							addEdge(b, b->pos+3);}}
			}

			if(b->type == hor){
				if(b->locY>0 && conf[b->pos-4]==-1 && conf[b->pos-3]==-1){
					addEdge(b, b->pos-4);
					if(b->locY>1 && conf[b->pos-8]==-1 && conf[b->pos-7]==-1){
						addEdge(b, b->pos-8);
						if(b->locY>2 && conf[b->pos-12]==-1 && conf[b->pos-11]==-1){
							addEdge(b, b->pos-12);
							if(b->locY>3 && conf[b->pos-16]==-1 && conf[b->pos-15]==-1)
								addEdge(b, b->pos-16);}}}

				if(b->locY<4 && conf[b->pos+4]==-1 && conf[b->pos+5]==-1){
					addEdge(b, b->pos+4);
					if(b->locY<3 && conf[b->pos+8]==-1 && conf[b->pos+9]==-1){
						addEdge(b, b->pos+8);
						if(b->locY<2 && conf[b->pos+12]==-1 && conf[b->pos+13]==-1){
							addEdge(b, b->pos+12);
							if(b->locY<1 && conf[b->pos+16]==-1 && conf[b->pos+17]==-1)
								addEdge(b, b->pos+16);}}}

				if(b->locX>0 && conf[b->pos-1]==-1){
					addEdge(b, b->pos-1);
					if(b->locX>1 && conf[b->pos-2]==-1)
						addEdge(b, b->pos-2);}

				if(b->locX<2 && conf[b->pos+2]==-1){
					addEdge(b, b->pos+1);
					if(b->locX<1 && conf[b->pos+3]==-1)
						addEdge(b, b->pos+2);}
			}

			if(b->type == ssq){
				if(b->locY>0 && conf[b->pos-4]==-1){
					addEdge(b, b->pos-4);
					if(b->locY>1 && conf[b->pos-8]==-1){
						addEdge(b, b->pos-8);
						if(b->locY>2 && conf[b->pos-12]==-1){
							addEdge(b, b->pos-12);
							if(b->locY>3 && conf[b->pos-16]==-1)
								addEdge(b, b->pos-16);}}}

				if(b->locY<4 && conf[b->pos+4]==-1){
					addEdge(b, b->pos+4);
					if(b->locY<3 && conf[b->pos+8]==-1){
						addEdge(b, b->pos+8);
						if(b->locY<2 && conf[b->pos+12]==-1){
							addEdge(b, b->pos+12);
							if(b->locY<1 && conf[b->pos+16]==-1)
								addEdge(b, b->pos+16);}}}

				if(b->locX>0 && conf[b->pos-1]==-1){
					addEdge(b, b->pos-1);
					if(b->locX>1 && conf[b->pos-2]==-1){
						addEdge(b, b->pos-2);
						if(b->locX>2 && conf[b->pos-3]==-1)
							addEdge(b, b->pos-3);}}

				if(b->locX<3 && conf[b->pos+1]==-1){
					addEdge(b, b->pos+1);
					if(b->locX<2 && conf[b->pos+2]==-1){
						addEdge(b, b->pos+2);
						if(b->locX<1 && conf[b->pos+3]==-1)
							addEdge(b, b->pos+3);}}
			}

			if(b->type == lsq){
				if(b->locY>0 && conf[b->pos-4]==-1 && conf[b->pos-3]==-1){
					addEdge(b, b->pos-4);
					if(b->locY>1 && conf[b->pos-8]==-1 && conf[b->pos-7]==-1){
						addEdge(b, b->pos-8);
						if(b->locY>2 && conf[b->pos-12]==-1 && conf[b->pos-11]==-1)
							addEdge(b, b->pos-12);}}

				if(b->locY<4 && conf[b->pos+8]==-1 && conf[b->pos+9]==-1){
					addEdge(b, b->pos+4);
					if(b->locY<3 && conf[b->pos+12]==-1 && conf[b->pos+13]==-1){
						addEdge(b, b->pos+8);
						if(b->locY<2 && conf[b->pos+16]==-1 && conf[b->pos+17]==-1)
							addEdge(b, b->pos+12);}}

				if(b->locX>0 && conf[b->pos-1]==-1 && conf[b->pos+3]==-1){
					addEdge(b, b->pos-1);
					if(b->locX>1 && conf[b->pos-2]==-1 && conf[b->pos+2]==-1)
						addEdge(b, b->pos-2);}

				if(b->locX<2 && conf[b->pos+2]==-1 && conf[b->pos+6]==-1){
					addEdge(b, b->pos+1);
					if(b->locX<1 && conf[b->pos+3]==-1 && conf[b->pos+7]==-1)
						addEdge(b, b->pos+2);}
			}
		}
	}
}

void addEdge(block* b, int newPos)
{
	conf[b->pos] = -1;
	if(b->type == hor || b->type == lsq) {conf[b->pos + 1] = -1;}
	if(b->type == ver || b->type == lsq) {conf[b->pos + 4] = -1;}
	if(b->type == lsq) {conf[b->pos + 5] = -1;}

	conf[newPos] = b->type;
	if(b->type == hor || b->type == lsq) {conf[newPos + 1] = b->type;}
	if(b->type == ver || b->type == lsq) {conf[newPos + 4] = b->type;}
	if(b->type == lsq) {conf[newPos + 5] = b->type;}

	updateGameState();
	uint64_t edge = currentGS.id;

	conf[newPos] = -1;
	if(b->type == hor || b->type == lsq) {conf[newPos + 1] = -1;}
	if(b->type == ver || b->type == lsq) {conf[newPos + 4] = -1;}
	if(b->type == lsq) {conf[newPos + 5] = -1;}

	conf[b->pos] = b->type;
	if(b->type == hor || b->type == lsq) {conf[b->pos + 1] = b->type;}
	if(b->type == ver || b->type == lsq) {conf[b->pos + 4] = b->type;}
	if(b->type == lsq) {conf[b->pos + 5] = b->type;}

	updateGameState();
	currentGS.edges.push_back(edge);
}

void solve(uint64_t gs)
{
	SE.pop();
	configureGameStateLite(gs);
	if(B[9].pos == 13) {
		won = true;
		winningState = gs;
		return;}
	findMoves();
	if(!currentGS.edges.empty())
		for(size_t i = 0; i < currentGS.edges.size(); i++)
			if(D.find(currentGS.edges[i]) == D.end())
			{
				D.insert(currentGS.edges[i]);
				SE.push(currentGS.edges[i]);
				DE.push_back(currentGS.edges[i]);
				PE.push_back(gs);
			}
}

void printConf()
{
	cout << "ID: " << currentGS.id << "\n";
	for(int i = 0; i < 5; i++)
	{
		for(int j = 0; j < 4; j++)
		{
			if(conf[4*i + j] == -1) {cout << "- ";}
			else {cout << conf[4*i + j] << " ";}
		}

		cout << "\n";
	}

	cout << "\n";
}

int main(int argc, char *argv[])
{
	/* TODO: add option to specify starting state from cmd line? */
	/* start SDL; create window and such: */
	if(!init()) {
		printf( "Failed to initialize from main().\n" );
		return 1;
	}
	atexit(close);
	bool quit = false; /* set this to exit main loop. */
	SDL_Event e;
	/* main loop: */
	while(!quit) {
		/* handle events */
		while(SDL_PollEvent(&e) != 0) {
			/* meta-q in i3, for example: */
			if(e.type == SDL_MOUSEMOTION) {
				if (mousestate == 1 && dragged) {
					int dx = e.button.x - lastm.x;
					int dy = e.button.y - lastm.y;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged->R.x += dx;
					dragged->R.y += dy;
				}
			} else if (e.type == SDL_MOUSEBUTTONDOWN) {
				if (e.button.button == SDL_BUTTON_RIGHT) {
					block* b = findBlock(e.button.x,e.button.y);
					if (b) b->rotate();
				} else {
					mousestate = 1;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged = findBlock(e.button.x,e.button.y);
				}
				/* XXX happens if during a drag, someone presses yet
				 * another mouse button??  Probably we should ignore it. */
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				if (e.button.button == SDL_BUTTON_LEFT) {
					mousestate = 0;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					if (dragged) {
						/* snap to grid if nearest location is empty. */
						snap(dragged);
					}
					dragged = NULL;
				}
			} else if (e.type == SDL_QUIT) {
				quit = true;
			} else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_ESCAPE:
					case SDLK_q:
						quit = true;
						break;
					case SDLK_LEFT:
						/* TODO: show previous step of solution */
						if(step < path.size()-2  && path.size() >= 1)
							configureGameState(path[++step]);
						break;
					case SDLK_RIGHT:
						/* TODO: show next step of solution */
						if(step > 0  && path.size() >= 1)
							configureGameState(path[--step]);
						break;
					case SDLK_p:
						printConf();
						/* TODO: print the state to stdout
						 * (maybe for debugging purposes...) */
						break;
					case SDLK_i:
						configureGameState(initialGS);
						break;
					case SDLK_f:
						findMoves();
						cout << "Found " << currentGS.edges.size() << " edges.\n";
						break;
					case SDLK_r:
						configureGameState(0);
						DE.clear();
						PE.clear();
						while(!SE.empty())
							SE.pop();
						D.clear();
						path.clear();
						won = false;

						break;
					case SDLK_s:
						/* TODO: try to find a solution */

						DE.clear();
						PE.clear();
						while(!SE.empty())
							SE.pop();
						D.clear();
						path.clear();
						won = false;

						originalState = currentGS.id;
						D.insert(currentGS.id);
						SE.push(currentGS.id);
						while(!won && !SE.empty())
							solve(SE.front());

						if(won)
						{
							configureGameState(winningState);
							path.push_back(winningState);
							uint64_t parent = winningState;

							while(parent != originalState)
							{
								for(size_t i = 0; i < DE.size(); i++)
									if(DE[i] == parent)
										parent = PE[i];

								path.push_back(parent);
							}

							path.push_back(parent);
							step = path.size() - 1;
							cout << "# of steps: " << path.size() - 2 << "\n";
						}

						if(!won)
							cout << "No Solution" << "\n";

						break;
					case SDLK_x:

						break;
					default:
						break;
				}
			}
		}
		fcount++;
		render();
	}

	printf("total frames rendered: %i\n",fcount);
	return 0;
}
